/*jslint browser: true*/
/*global $, jQuery, alert*/
$(document).ready(function () {
    "use strict";

    /** Variables **/
    var value, p, w, all_tprice, all_lignes, all_tGo, moyenneGo,
        all_us_tprice, all_us_lignes, all_us_tGo, moyenneGo_us,
        a, b, c, d, all_prices, all_totallignes, moy_total;

    var lang = $('html').attr('lang');


    /** functions **/
    function rounded(v) {
            return v.toFixed(2).toString().replace('.', ',');
    }
    function roundedperlang(v,n) {
        if(lang == 'en'){
            return v.toFixed(n).toString();
        }
        if(lang == 'fr'){
            return v.toFixed(n).toString().replace('.', ',');
        }
    }

    function formated(t) {
        var f;
        f = Number(t.text().replace(',', '.'));
        return f;
    }

    /****************/


    $('input').keyup(function () {
        //Default value form input
        value = $(this).val();

        /** Calcul du 'Coût total' par ligne */
        p = $(this).data('price');
        if(isNaN(value * p)){
            $(this).parents('tr').eq(0).find('.tprice span').text('N/A');
        }else{
            $(this).parents('tr').eq(0).find('.tprice span').text(roundedperlang(value * p,2));
            $(this).parents('tr').eq(0).find('.tprice').data('tprice', value * p);
        }



        /** Calcul du 'Total de Go / mois' */
        w = $(this).data('weight');
        if(isNaN(value * w)){
            $(this).parents('tr').eq(0).find('.tGo span').text('N/A');
        }else{
            $(this).parents('tr').eq(0).find('.tGo span').text(value * w);
        }


        /** Update la value de la dernière colonne **/
        var mo = w;
        if(lang == 'fr'){
            mo = mo.toFixed(1).toString().replace('.', ',');
            $(this).parents('tr').eq(0).find('.moyMo span').text(mo);
        }else{
            $(this).parents('tr').eq(0).find('.moyMo span').text(w);
        }
        $(this).parents('tr').eq(0).find('.moyGo span').text(w);

        if(w == '0.5'){
        $('.the_moyMo span').text('500');
        }



        /** Somme des "Coût total" -> 'Total affaires partage Canada vs price canadien seulement */
        all_tprice = 0;
        $('tr.all_ca').find('.tprice').each(function () {
            all_tprice += $(this).data('tprice');
        });
        if(isNaN(all_tprice)){
            $('tr.all_ca').find('.stp_ca span').text('N/A');
        }else{
            $('tr.all_ca').find('.stp_ca span').text(roundedperlang(all_tprice,2));
            $('tr.all_ca').find('.stp_ca').data('stpca', all_tprice);
        }
        if( lang == 'en'){
            $('tr.all_ca').find('.stp_ca span').currency();
            var t = $('tr.all_ca').find('.stp_ca span').text().replace('$','');
            $('tr.all_ca').find('.stp_ca span').text(t);
        }

        /** Somme des "Lignes requises" -> 'Total affaires partage Canada vs price canadien seulement */
        all_lignes = 0;
        $('tr.all_ca').find('input').each(function () {
            all_lignes += Number($(this).val());
        });
        if(isNaN(all_lignes)){
            $('tr.all_ca').find('.lignes_ca span').text('N/A');
        }else{
            $('tr.all_ca').find('.lignes_ca span').text(all_lignes);
        }


        /** Somme des "Coût total" -> 'Total affaires partage Canada vs price canadien seulement */
        all_tGo = 0;
        $('tr.all_ca').find('.tGo span').each(function () {
            all_tGo += formated($(this));
        });
        if(lang == 'fr'){
            $('tr.all_ca').find('.stGo_ca span').text(all_tGo.toString().replace('.',','));
        }
        if(lang == 'en'){
            $('tr.all_ca').find('.stGo_ca span').text(all_tGo);
        }


        /** Calcul de la "moyenne de Go par ligne /mois" */
        moyenneGo = 0;
        a = $('.stGo_ca span').text().replace(',', '.');
        b = $('.lignes_ca span').text().replace(',', '.');
        moyenneGo = Number(a / b);
        if(isNaN(moyenneGo)){
            $('tr.all_ca').find('.subMoyenne span').text('N/A');
        }else{
            $('tr.all_ca').find('.subMoyenne span').text(roundedperlang(moyenneGo,2));
        }



        /** US CA **/
        /** Somme des "Coût total" -> 'Total affaires partage Canada vs price canadien seulement */
        all_us_tprice = 0;
        $('tr.all_us_ca').find('.tprice').each(function () {
            all_us_tprice +=  $(this).data('tprice');
        });
        if(isNaN(all_us_tprice)){
            $('tr.all_us_ca').find('.stp_us_ca span').text('N/A');
        }else{
            $('tr.all_us_ca').find('.stp_us_ca span').text(roundedperlang(all_us_tprice,2));
            $('tr.all_us_ca').find('.stp_us_ca').data('stpusca', all_us_tprice);
        }
        if( lang == 'en'){
            $('tr.all_us_ca').find('.stp_us_ca span').currency();
            var t =  $('tr.all_us_ca').find('.stp_us_ca span').text().replace('$','');
            $('tr.all_us_ca').find('.stp_us_ca span').text(t);
        }

        /** Somme des "Lignes requises" -> 'Total affaires partage Canada vs price canadien seulement */
        all_us_lignes = 0;
        $('tr.all_us_ca').find('input').each(function () {
            all_us_lignes += Number($(this).val().replace(',', '.'));
        });
        if(isNaN(all_us_lignes)){
            $('tr.all_us_ca').find('.lignes_us_ca span').text('N/A');
        }else{
            $('tr.all_us_ca').find('.lignes_us_ca span').text(all_us_lignes);
        }


        /** Somme des "Coût total" -> 'Total affaires partage Canada vs price canadien seulement */
        all_us_tGo = 0;
        $('tr.all_us_ca').find('.tGo span').each(function () {
            all_us_tGo += parseInt($(this).text());
        });
        if(isNaN(all_us_tGo)){
            $('tr.all_us_ca').find('.stGo_us_ca span').text('N/A');
        }else{
            $('tr.all_us_ca').find('.stGo_us_ca span').text(all_us_tGo);
        }



        /** Calcul de la "moyenne de Go par ligne /mois" */
        moyenneGo_us = 0;
        c = $('.stGo_us_ca span').text().replace(',', '.');
        d = $('.lignes_us_ca span').text().replace(',', '.');
        moyenneGo_us = Number(c / d);
        moyenneGo_us = (isNaN(moyenneGo_us)) ? 0 : moyenneGo_us;
        if(isNaN(moyenneGo_us)){
            $('tr.all_us_ca').find('.subMoy_us_ca span').text('N/A');
        }else{
            $('tr.all_us_ca').find('.subMoy_us_ca span').text(roundedperlang(moyenneGo_us,1));
        }


        /** specific for replace . by , in FR | only for the value MO **/
        var the_Mo = $('.theMO span').text();
        if(lang == 'fr'){
            if(the_Mo.indexOf('.') != -1){
                var v = the_Mo.toString().replace('.', ',');
                $('.theMO span').text(v);
            }
        }

        if( lang == 'en'){
            $(this).parents('tr').eq(0).find('.tprice span').currency();
            var t = $(this).parents('tr').eq(0).find('.tprice span').text().replace('$','');
            $(this).parents('tr').eq(0).find('.tprice span').text(t);
        }



        /** super total **/
        var l1 = Number($('.lignes_ca span').text().replace(',', '.')),
            l2 = Number($('.lignes_us_ca span').text().replace(',', '.')),
            p1 = $('.stp_ca').data('stpca'),
            p2 = $('.stp_us_ca').data('stpusca');


        all_totallignes = Number(l1 + l2);
        all_prices = Number(p1 + p2);
        moy_total = Number(all_prices / all_totallignes);


        all_prices = (isNaN(all_prices)) ? 0 : all_prices;
        all_totallignes = (isNaN(all_totallignes)) ? 0 : all_totallignes;
        moy_total = (isNaN(moy_total)) ? 0 : moy_total;


//        console.log(all_prices +' '+all_totallignes+' '+moy_total);

        $('tbody').find('.total_lignes span').text(all_totallignes);
        $('tbody').find('.total_prices span').text(roundedperlang(all_prices,2));
        $('tbody').find('.moy_total span').text(roundedperlang(moy_total,2));

        if( lang == 'en'){
            $('tbody').find('.total_prices span').currency();
            var t = $('tbody').find('.total_prices span').text().replace('$','');
            $('tbody').find('.total_prices span').text(t);
        }

    });





    //Print
    $('.outprint').click(function(){
        window.print();
    });


});