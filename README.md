#Videotron - Calculateur Affaires Partage

## Général
----
##### Code du projet :
15_1025_VTL_MI_Outil_forfait_partage_affaire_NL
##### Description :
Créer le Tableau Calculateur pour les forfaits mobilité affaires partagés


## Leads de projet
----
##### Frontend :
`Ingrid Schwietzer`

( [ingrid.schwietzer@nurun.com] )
[ingrid.schwietzer@nurun.com]: mailto:ingrid.schwietzer@nurun.com

##### Chargé de projet :
`Jani Bouchard`

( [jani.bouchard@nurun.com] )
[jani.bouchard@nurun.com]: mailto:jani.bouchard@nurun.com


#### Environnements
Environnement totalement static (no Grunt, no Less...)